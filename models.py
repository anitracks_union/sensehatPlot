# Implements the models for the piCamSQL.
#
# Seth McNeill
# 2016 April 07

import config
import datetime
from peewee import *

db = MySQLDatabase(config.DATABASE['db'], host=config.DATABASE['host'],
  user=config.DATABASE['user'], passwd=config.DATABASE['passwd'])

class BaseModel(Model):
    class Meta:
        database = db

# timestamp ideas from https://github.com/coleifer/peewee/blob/master/docs/peewee/models.rst
class Pressure(BaseModel):
  timestamp = DateTimeField(default=datetime.datetime.now)
  pressure = FloatField()

class Humidity(BaseModel):
  timestamp = DateTimeField(default=datetime.datetime.now)
  humidity = FloatField()

class Temperature(BaseModel):
  timestamp = DateTimeField(default=datetime.datetime.now)
  temperatureC = FloatField()
  temperatureF = FloatField()

class Compass(BaseModel):
  timestamp = DateTimeField(default=datetime.datetime.now)
  north = FloatField()

class Orientation(BaseModel):
  timestamp = DateTimeField(default=datetime.datetime.now)
  pitch = FloatField()
  roll = FloatField()
  yaw = FloatField()

db.create_tables([Pressure, Humidity, Temperature, Compass, Orientation], safe=True)
