#!/usr/bin/python
#
# Collects data from the Sensehat and saves it to a MySQL database.
#
# Seth McNeill
# Union College
# 2016 April 10

from sense_hat import SenseHat
import config
import models
from peewee import *

sense = SenseHat()
humidity = sense.get_humidity()
tempC = sense.get_temperature()
pressure = sense.get_pressure()
tempF = tempC*1.8 + 32.0
north = sense.get_compass()
orientation = sense.get_orientation()

#print("h = %s, tempC = %s, press = %s" % {humidity, tempC, pressure})
print("Humidity: %s %%rH" % humidity)
print("Temperature: %s C" % tempC)
print("Pressure: %s Millibars" % pressure)
print("p: {pitch}, r: {roll}, y: {yaw}".format(**orientation))
print("North: %s" % north)

models.Humidity.create(humidity=humidity)
models.Pressure.create(pressure=pressure)
models.Temperature.create(temperatureC=tempC, temperatureF=tempF)
models.Compass.create(north=north)
models.Orientation.create(pitch=orientation['pitch'], roll=orientation['roll'], yaw=orientation['yaw'])
