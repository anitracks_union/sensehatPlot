<?php 
// collects the environmental data and prints it for dygraph use
// Seth McNeill, 2016 April 10
header('Content-type: text/html; Charset=utf-8'); // to make it a text file

$servername = "localhost";
$username = "sensehat";
$password = "putpasswordhere";
$dbname = "sensehat";
$table = "humidity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

echo "Date, Humidity\n";
$sql = "SELECT * FROM $table ORDER BY timestamp";
if($result = $conn->query($sql)) // query was successful
{
//  $row_cnt = $result->num_rows;
  while($row = mysqli_fetch_assoc($result))
  {
    echo $row['timestamp'] . ',' . $row['humidity'] . "\n" ;
  }
  $result->close();
}
$conn->close();
